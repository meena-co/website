.POSIX:

tidy!=(which tidy5 || which tidy)

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' ${.MAKE.MAKEFILES} $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

update-static: ## build and tidy website
	rm -rf site/
	env TZ=UTC MIX_ENV=prod mix serum.build
	$(tidy) -config tidy.config site/*.html || true
	$(tidy) -config tidy.config site/thoughts/*.html || true

.PHONY: serve
serve: ## serve site directory with builtin python http.server
	python3 -m http.server --directory site/
