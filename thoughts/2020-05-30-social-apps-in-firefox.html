<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="author" content="Mina Galić">
    <meta name="description" content="Infrastructure &amp; Open Source">
    <meta property="og:description" content="Infrastructure &amp; Open Source">
    <title>Mina Galić - How I run Social Apps in Firefox</title>
    <meta property="og:title" content="Mina Galić - How I run Social Apps in Firefox">
    <meta property="og:image" content="/media/igalic4.jpeg">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://igalic.co/thoughts/2020-05-30-social-apps-in-firefox.html">
    <link rel="stylesheet" href="/assets/fonts/bitter.css">
    <link rel="stylesheet" href="/assets/css/tufte.css">
    <link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>
    <section>
        <h1>How I run Social Apps in Firefox</h1>

        <p><label for="mn-av" class="margin-toggle">​</label> <input type="checkbox" id="mn-av" class=
        "margin-toggle" checked="true"> <span class="marginnote"><img id="av" src="/media/igalic4.webp" alt=
        "Mina's face in profile: She's wearing a sun hat" width="307" height="306"></span></p>
    </section>

    <section>
        <nav>
            <ul>
                <li>
                    <a href="/index.html">Me</a>
                </li>

                <li aria-hidden="true">|</li>

                <li>
                    <a href="/cv.html">My Work</a>
                </li>

                <li aria-hidden="true">|</li>

                <li>
                    <a href="/thoughts/">Blog</a>
                </li>
            </ul>
        </nav>
    </section>

    <hr>

    <article>
        <section>
            <p>Posted on Saturday, 30 May 2020 by Mina Galić</p>

            <p>A long time ago I stopped using Chrome. And then Electron showed up. It seemed like a very
            convinient way to run all these Social Apps. Tools like <a href="https://meetfranz.com/">Franz</a>
            and <a href="https://rambox.pro/">Rambox</a> seemed like a good way to contain them.</p>

            <p>Running more than one browser under restricted memory conditions (8GiB) was problematic: It was
            impossible to run VSCode and Franz / Rambox at the same time. Starting Firefox before one of these
            would lock up all tabs, making the… <em>browser</em> browser effectively useless.</p>

            <p>I needed to change something.</p>

            <p>Let’s back up here, what’s the problem with Electron (other than using off all my memory and
            killing off my browser)?</p>

            <p>Electron isn’t just Chrome. It’s also NodeJS. That means it doesn’t just give you access to the
            Internet, it also has full access to your filesystem. And that means if the programmers of Discord
            mess up somewhere, they could wipe your filesystem. That cannot happen if you’re just using
            discord as pure Web Application.</p>

            <p>Running multiple different Electron Applications on the same memory-restricted systems means
            that your OS cannot even do the bare-minimum of only loading the shared libraries once for
            multiple running instances. This is because each one of those application has its own
            release-cycle and as such, very different bundled versions of shared libraries.</p>

            <p>The memory issues, and the security risks combined gave me the push to look for
            alternatives.</p>

            <p>Firefox has a couple of features that all combined allow to do what Rambox / Franz do, without
            having to run Electron:</p>

            <ul>
                <li>
                    <a href="https://support.mozilla.org/en-US/kb/containers">Containers</a>
                </li>

                <li>
                    <a href=
                    "https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles">
                    Profiles</a>
                </li>

                <li>
                    <a href=
                    "https://support.mozilla.org/en-US/kb/pinned-tabs-keep-favorite-websites-open">Pinned
                    tabs</a>
                </li>

                <li>and as an extra: <a href="https://www.mozilla.org/en-US/firefox/sync/">Firefox Sync</a>
                </li>
            </ul>

            <p>and the fact that we’re usning just a single version of Firefox means the OS <em>can</em>
            optimise loading of shared libraries of your multiple instances.</p>

            <h2 id="how?">How?</h2>

            <p>So, how do we do this?</p>

            <p>We’ll create a fresh profile, and call it “social”</p>

            <pre><code class="sh">% firefox --ProfileManager</code></pre>
            <p>and create a “Desktop” entry in <code class=
            "inline">~/.local/share/applications/social.desktop</code> to make it easily accessible:</p>

            <pre><code class="ini">[Desktop Entry]
Version=1.0
Name=Social
Comment=Social Media Profile for Firefox
GenericName=Web Browser
Keywords=Internet;WWW;Browser;Web;Explorer;Social
Exec=firefox -P social %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=network-workgroup
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/chrome;video/webm;application/x-xpinstall;
StartupNotify=true</code></pre>
            <p>This creates us an empty profile and makes it accessible via application menues under the name 
            “social”.</p>

            <p>The most important line here is:</p>

            <pre><code class="ini">Exec=firefox -P social %u</code></pre>
            <p>But <code class="inline">Name=</code> and <code class="inline">Comment=</code> make it easy to
            find in menus.</p>

            <p>From here, we need to install a couple of useful plugins. I don’t use any browser without
            <a href="https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/">uBlock Origin</a>.</p>

            <p>For what we need to accomplish, we’ll also need <a href=
            "https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/">Firefox Multi-Account
            Containers</a>. If you’re using Twitter, or Facebook or LinkedIn or others, they have specialized
            container addons.</p>

            <p>What these addons do is capture all known URLs of a service to guarantee that they are opened
            in the dedicated container. The containers are session and cookie barriers, and since we’re
            planning to run every service in a dedicated container there’s no cross-pollination… or poisoning
            happening.</p>

            <h2 id="let’s-do-it!">Let’s do it!</h2>

            <p>We can now start adding service by service, making sure each is running in a dedicated
            container with a nice colour we like. Once the setup (well, sign-up, really) is complete, we can
            <em>pin</em> the tab:</p>

            <p><img src="/assets/social-firefox/pinned-tabs.webp" alt=
            "A crop of the pinned tabs of my setup, showing 13 of them">
            </p>

            <p>We now start using our social Firefox. As we do, we’ll notice that we keep wanting to open
            links in our regular browser, or vice-versa.</p>

            <h2 id="firefox-sync">Firefox Sync</h2>

            <p>And we can use Firefox Sync to do that! By making our social profile its own Firefox Sync 
            “device”, we can easily send links from and to these devices:</p>

            <p><img src="/assets/social-firefox/send-link-to-device.webp" alt=
            "Firefox Context Menus, pointing at Send to Device and showing available devices">
            </p>

            <p>If you take care about naming your devices, sending links can become a very quick action. From
            the keyboard, <code class="inline">[Send Link to Device]</code> is under <code class=
            "inline">n</code>.</p>

            <p>Note that Firefox Sync allows you to very fine-tune what to sync, and for this profile, I chose
            to sync <em>nothing</em>.</p>

            <h2 id="reducing-surface">Reducing Surface</h2>

            <p>One of the goals of this project was to provide an “appliance” feeling, for that purpose it’s
            necessary to reduce the surface and streamline the experience:</p>

            <p>uBlock’s Settings allows to disable its context menu entry:</p>

            <ul>
                <li>☐ Make use of context menu where appropriate</li>
            </ul>

            <p>We can disable Pocket. We can do that in <code class="inline">about:config</code> by setting
            <code class="inline">extensions.pocket.enabled</code> to <code class="inline">false</code></p>

            <p>That gets rid of the Context Menu Item <code class="inline">[Save Link to Pocket]</code>.</p>

            <p>We can also disable Developer Tools, by setting everything in <code class=
            "inline">devtools.*.enable</code> to <code class="inline">false</code>.</p>

            <p>That gets rid of two Context Menu Items: <code class="inline">[Inspect Accessibility
            Properties]</code> and <code class="inline">[Inspect (Q)]</code>.</p>

            <p>That is seemed all that was possible for now, so for the future, I opened two bugs for further
            slimming down Firefox:</p>

            <ul>
                <li>
                    <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1704198">Disable “Search” in context
                    menu</a>
                </li>

                <li>
                    <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1704200">Allow disabling Private
                    browsing per profile</a>
                </li>
            </ul>

            <p>But it turns out that we <em>can</em> disable Private Browsing. Let’s go ahead and modify our
            <code class="inline">.desktop</code> file to add an environment variable to the startup:</p>

            <pre><code class="ini">Exec=env XPCSHELL_TEST_PROFILE_DIR=1 firefox -P social %u</code></pre>
            <p>And before we restart, we an already modify <code class="inline">about:config</code> to add a
            string parameter, <code class="inline">browser.policies.alternatePath</code>, which we’ll point at
            our <code class="inline">policies.json</code>. I put mine into the profile folder itself, and
            filled it with:</p>

            <pre><code class="json">{
  "policies": {
    "DisablePrivateBrowsing": true
  }
}</code></pre>
            <p>After restarting our “social” Firefox, the context menu now looks like:</p>

            <p><img src="/assets/social-firefox/context-menu-without-private.web" alt=
            "Firefox context menu after clicking on a Link, There is no longer an &quot;Open Link in New Private Window&quot; item">
            </p>

            <p>You can take a look at <a href="https://github.com/mozilla/policy-templates/">the other
            policies</a> for what you might want to disable in your profile.</p>

            <p>If you find more ways of slimming down Firefox, or improving experience I’d love to hear from
            you, until then, happy socialising!</p>
        </section>

        <hr>

        <section>
            <p>Tags:</p>

            <ul class="tags">
                <li>
                    <a href="/tags/containers">containers</a>
                </li>

                <li aria-hidden="true">,</li>

                <li>
                    <a href="/tags/electron">electron</a>
                </li>

                <li aria-hidden="true">,</li>

                <li>
                    <a href="/tags/firefox">firefox</a>
                </li>

                <li aria-hidden="true">,</li>

                <li>
                    <a href="/tags/socia%20web%20apps">socia web apps</a>
                </li>
            </ul>
        </section>
    </article>
</body>
</html>
